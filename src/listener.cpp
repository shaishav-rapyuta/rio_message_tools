#include "ros/ros.h"
#include "std_msgs/String.h"
#include <time.h>

using namespace std;

double counter = 0.0;
double prev_value = -1.0;
time_t start = time(0);

bool isInflection( double current,  double previous)
{
   if(current < previous)
        return true;
   else
	return false;
}

double timeDiff(){
   double seconds_since_start = difftime( time(0), start);
}


bool isBeyondOneCycle(double diff, double RESET_DURATION) {
    if(diff >= RESET_DURATION)
    {
	start = time(0);
	return true;
    }
    else 
    {
	return false;
    }
}


double SAMPLE_DURATION=5;
void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
  double curr;
  sscanf(msg->data.c_str(), "%lf", &curr);
  double time_diff = timeDiff(); 
  
  //if(isInflection(curr, prev) || 
  if(isBeyondOneCycle(time_diff, SAMPLE_DURATION))
  {
  	  ROS_INFO("I heard : [%lf] messages in [%lf] seconds. Rate: [%lf]Hz", counter, time_diff, counter/time_diff);
	  counter = 0.0;
  }
  else {
	  counter = counter + 1.0;
  }
  prev_value = curr;
}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "listener");
  ros::NodeHandle n;
  n.getParam("sample_duration",SAMPLE_DURATION);
  ros::Subscriber sub = n.subscribe("topic", 1000, chatterCallback);
  ros::spin();
  return 0;
}
