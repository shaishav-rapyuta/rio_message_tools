#include "ros/ros.h"
#include "std_msgs/String.h"
#include "include/json.hpp"
#include <sstream>
// for convenience
using json = nlohmann::json;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "rio_topic_graph");
  ros::NodeHandle n;
  ros::Publisher topic_graph_pub = n.advertise<std_msgs::String>("rio_topic_graph", 0);

  ros::Rate loop_rate(0.25);

  while (ros::ok())
  {
    ros::master::V_TopicInfo advertized_topics;
    ros::master::getTopics(advertized_topics);
    json topic_graph = json::object();
    for (const auto& topic_info: advertized_topics)
    {
	    topic_graph[topic_info.name.c_str()] = topic_info.datatype.c_str();
    }
    
    std_msgs::String msg;
    msg.data = topic_graph.dump(2);
    ROS_INFO("%s", msg.data.c_str());
    topic_graph_pub.publish(msg);
    
    ros::spinOnce();
    loop_rate.sleep();
  }


  return 0;
}
